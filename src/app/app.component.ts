import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { CacheService } from 'ionic-cache';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Graph',
      url: '/graph',
      icon: 'analytics'
    },
    {
      title: 'Infos and Updates',
      url: '/updates',
      icon: 'alert'
    }
  ];

  darkMode = true;

  constructor(
    private platform: Platform,
    private cache: CacheService,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.cache.setDefaultTTL(60 * 60 * 24 * 365);
      this.cache.setOfflineInvalidate(false);
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');
      this.darkMode = prefersDark.matches;
    });
  }

  useDarkTheme() {
    console.log('usedarkTheme Called with darktheme:' + this.darkMode);

    document.body.classList.toggle('dark');
    this.darkMode = !this.darkMode;
  }
}
