import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-buttons',
  templateUrl: './buttons.component.html',
  styleUrls: ['./buttons.component.scss'],
})
export class ButtonsComponent implements OnInit {

  @Output() clickEvent = new EventEmitter<Object>();
  @Output() drawEvent = new EventEmitter<string>();

  private mode: number;
  drawOption = '';
  operations = '';

  constructor(private actionSheetController: ActionSheetController) {
    this.mode = 0;
  }

  ngOnInit() {}

  drawVector() {

    this.clickEvent.emit('Draw');
  }

  clearGrid() {

    this.clickEvent.emit('Clear');
  }

  async presentActionSheet() {

    const actionSheet = await this.actionSheetController.create({
      header: 'Operations',
      cssClass: 'operationSheet',
      buttons: [{
        text: 'Single Vector',
        icon: 'tablet-landscape',
        handler: () => {
          this.mode = 0;
          this.operations = 'single';
          this.clickEvent.emit(this.mode);
        }
      }, {
        text: 'Addition',
        icon: 'add',
        handler: () => {
          this.operations = 'addition';
          this.mode = 1;
          this.clickEvent.emit(this.mode);
        }
      }, {
        text: 'Subtraction',
        icon: 'Remove',
        handler: () => {
          this.operations = 'subtraction';
          this.mode = 2;
          this.clickEvent.emit(this.mode);
        }
      }, {
        text: 'Multiplication',
        icon: 'close',
        handler: () => {
          this.operations = 'multiplication';
          this.mode = 3;
          this.clickEvent.emit(this.mode);
        }
      }, {
        text: 'Division',
        icon: 'calculator',
        handler: () => {
          this.operations = 'division';
          this.mode = 4;
          this.clickEvent.emit(this.mode);
        }
      }, {
        text: 'Return',
        icon: 'return-left',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
    }

  async drawOptionsChanged($event) {
    console.log($event.detail);

    const actionSheet = await this.actionSheetController.create({
        header: 'Draw Options',
        cssClass: 'operationSheet',
        buttons: [{
          text: 'single',
          icon: 'checkmark',
          handler: () => {
            this.drawOption = '';
            this.drawEvent.emit('');
          }
        }, {
          text: 'complex conjugate',
          icon: 'checkmark',
          handler: () => {
            this.drawOption = 'conjugate';
            this.drawEvent.emit('c');
          }
        }, {
          text: 'complex inverse',
          icon: 'checkmark',
          handler: () => {
            this.drawOption = 'inverse';
            this.drawEvent.emit('i');
          }
        }, {
          text: 'complex conjugate + inverse',
          icon: 'checkmark',
          handler: () => {
            this.drawOption = 'conjugate + inverse';
            this.drawEvent.emit('ci');
          }
        },
          {
          text: 'Return',
          icon: 'return-left',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
      });
    await actionSheet.present();
  }
}
