import { Component, AfterViewInit, ViewChild, EventEmitter, Output } from '@angular/core';
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { Text } from 'troika-three-text';

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.scss'],
})
export class CanvasComponent implements AfterViewInit {

@ViewChild('cartesianGraph', {static: false}) graph: any;

  private canvasElement: any;
  private renderer: any;
  private camera: any;
  private controls: any;
  private scene: any;

  private ctx: any;
  private width: any;
  private height: any;

  @Output() sceneEvent = new EventEmitter<THREE.Scene>();

  constructor() { }


  ngAfterViewInit() {

    this.canvasElement = this.graph.nativeElement;
    this.ctx = this.canvasElement.getContext( 'webgl2', { alpha: false } );
    // this.width = window.innerWidth;
    // this.height = window.innerHeight * 0.6;
    this.width = window.screen.width;

    this.height = window.screen.height > 1024 ? window.screen.height * 0.6 : window.screen.height * 0.5;

    this.setScene();

    this.setLight();

    this.setCamera();

    this.drawGrid();

    // this.drawAxes();

    this.setRenderer();

    this.setControlElements();

    this.render();

  }

  render() {

    try {

      requestAnimationFrame(this.render.bind(this));
      this.renderer.render(this.scene, this.camera);
    } catch (e) {}
  }

  onResize() {

    this.camera.aspect = (this.width) / (this.height);
    this.camera.updateProjectionMatrix();
    this.renderer.setSize( this.width,
                           this.height);
    this.render();
  }

  setScene() {

    this.scene = new THREE.Scene();
    this.scene.background = new THREE.Color( 0xececec );
    this.sceneEvent.emit(this.scene);
  }

  setLight() {

    const color = 0x00ff44;
    const intensity = 1;
    const light = new THREE.DirectionalLight(color, intensity);
    light.position.set(0.0001, -1, 0.0001);
    this.scene.add(light);
  }

  setCamera() {

    const fov = 75;
    const aspect = this.width / this.height;  // the canvas default
    const left = - this.width / 64;
    const right = - left;
    const top = this.height / 64 ;
    const bottom = - top;
    const near = .1;
    const far = 1000;
    this.camera = new THREE.OrthographicCamera(left, right, top, bottom, near, far);
    // const camera = new THREE.OrthographicCamera( -this.width , this.width , this.height, -this.height, 1, 1000 );
    // this.camera = new THREE.PerspectiveCamera(fov, aspect, near, far);

    this.camera.position.set( 0.0001, 100, (this.width >= 1024 ? 125 : 50) );
    // this.camera.position.set(0, 100, (this.width >= 1024 ? 130 : 50));
    this.camera.lookAt( 0, 0, 0 );
  }

  resetCameraPosition() {

    this.camera.position.set( 0.0001, 100, (this.width >= 1024 ? 125 : 50) );
    // this.camera.position.set(0, 2000, 0);
    this.camera.lookAt( 0, 0, 0 );
    this.controls.reset();
    this.controls.update();
  }

  setRenderer() {

    this.renderer = new THREE.WebGLRenderer({canvas: this.canvasElement, context: this.ctx, antialias: true});
    this.renderer.setPixelRatio( window.devicePixelRatio );
    this.renderer.setSize( this.width, this.height );
  }

  setControlElements() {

    this.controls = new OrbitControls( this.camera, this.renderer.domElement );
    this.controls.enableRotate = false;
    this.controls.mouseButtons = {
      MIDDLE: THREE.MOUSE.DOLLY,
      LEFT: THREE.MOUSE.PAN
    };
    this.controls.touches = {
      ONE: THREE.TOUCH.PAN,
      TWO: THREE.TOUCH.DOLLY_PAN
    };
    this.controls.addEventListener( 'change', this.render );
    this.controls.update();

    window.addEventListener( 'resize', onresize, false );
  }

  drawGrid() {

    const Xlines = [];
    const Ylines = [];

    for (let i = -200; i <= 200; ++i) {
      if (i !== 0) {
        const Xpoints = [];

        Xpoints.push( new THREE.Vector3( -0.25, 0.0005, i ) );
        Xpoints.push( new THREE.Vector3( 0.25, 0.0005, i ) );

        const Xline = new THREE.LineSegments( new THREE.BufferGeometry().setFromPoints(Xpoints) ,
          new THREE.LineDashedMaterial( {
          color: 0x000000, // 0xff0033
          linewidth: 2,
          scale: 1,
          dashSize: 0,
          gapSize: 0
          })
        );

        Xlines.push(Xline);

        const Ypoints = [];

        Ypoints.push( new THREE.Vector3( i, 0.0005, -0.25 ) );
        Ypoints.push( new THREE.Vector3( i, 0.0005, 0.25 ) );

        const Yline = new THREE.LineSegments( new THREE.BufferGeometry().setFromPoints(Ypoints) ,
          new THREE.LineDashedMaterial( {
          color: 0x000000,
          linewidth: 2,
          scale: 1,
          dashSize: 0,
          gapSize: 0
          })
        );

        Ylines.push(Yline);
      }
    }

    Ylines.forEach(e => {

      e.computeLineDistances();
      this.scene.add(e);
    });

    Xlines.forEach(e => {

      e.computeLineDistances();
      this.scene.add(e);
    });

    // possible 5c5d60 or fffcf2
    const grid = new THREE.GridHelper( 400, 400, 0x000000, 0xfffcf2 );
    this.scene.add( grid );
    this.loadText(0, 1, 'i');
    this.loadText(1, 0, '1');
  }

  clearGrid() {

    this.scene.dispose();
    this.ngAfterViewInit();
  }

  drawAxes() {

    let dir = new THREE.Vector3( 1, 0.0005, 0);
    const origin = new THREE.Vector3( 0 , 0.0005, 0 );
    const length = 1;
    let hex = 0xff0000;

    const xAxis = new THREE.ArrowHelper( dir, origin, length, hex );
    this.scene.add(xAxis);

    dir = new THREE.Vector3( 0, 0.0005, -1);
    hex = 0x0000ff;
    const yAxis = new THREE.ArrowHelper( dir, origin, length, hex );
    this.scene.add(yAxis);
  }

  loadText(x: number, y: number, t: string) {

    const text = new Text();

    text.text = t;
    text.fontSize = .4;
    text.position.y = 0;
    text.position.z = - y; // neg up , pos down
    text.position.x = x + .1; // left right
    text.rotation.x = - Math.PI / 2;
    text.color = 0x000000;
    text.sync();
    this.scene.add(text);
  }
}
