import { ModeswitchComponent } from './modeswitch/modeswitch.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { GraphComponent } from './graph/graph.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { InputsComponent } from './inputs/inputs.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CanvasComponent } from './canvas/canvas.component';
import { VectormanagerComponent } from './vectormanager/vectormanager.component';
import { ColorPickerModule } from 'ngx-color-picker';
import { NthrootComponent } from './nthroot/nthroot.component';
import { StepByStepToolComponent } from './stepbysteptool/stepbysteptool.component';


@NgModule({
    imports: [CommonModule, FormsModule, ReactiveFormsModule, ColorPickerModule],
    declarations: [GraphComponent, ModeswitchComponent, ButtonsComponent,
                    InputsComponent, CanvasComponent, VectormanagerComponent, NthrootComponent, StepByStepToolComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    exports: [GraphComponent, ModeswitchComponent, ButtonsComponent,
             InputsComponent, CanvasComponent, VectormanagerComponent, NthrootComponent, StepByStepToolComponent]
})
export class ComponentsModule {}
