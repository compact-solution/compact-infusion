import { Component, Input, AfterViewInit, ViewChild, ViewChildren, QueryList, ChangeDetectorRef, ɵNG_INJ_DEF } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { VectorManagerService } from 'src/app/services/vectormanager.service';
import { InputsComponent } from '../inputs/inputs.component';
import { CanvasComponent } from '../canvas/canvas.component';
import { VectormanagerComponent } from '../vectormanager/vectormanager.component';
import { NthrootComponent } from '../nthroot/nthroot.component';
import { StepByStepToolComponent } from '../stepbysteptool/stepbysteptool.component';


@Component({
  selector: 'app-graphinputs',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss'],
})
export class GraphComponent implements AfterViewInit {

  @ViewChild(CanvasComponent) canvas: CanvasComponent;
  @ViewChild(VectormanagerComponent) vectorlist: VectormanagerComponent;
  @ViewChildren(InputsComponent) inputs: QueryList<InputsComponent>;
  @ViewChild(InputsComponent) input: InputsComponent;
  @ViewChild(NthrootComponent) nthroot: NthrootComponent;
  @ViewChild(StepByStepToolComponent) sbstool: StepByStepToolComponent;
  @ViewChildren(VectormanagerComponent) vectorElements: QueryList<VectormanagerComponent>;


  isDisabled = false;
  inv = false;
  conj = false;
  operations = 0;
  opnumbers: Map<string, number[]>;
  operator = '';
  components = [];
  mode: number;
  drawOptions = [];

  @Input() real: any;
  @Input() imaginary: any;

  @Input() modulus: any;
  @Input() phase: any;

  @Input() root: number;

  mod: any;
  pha: any;
  re: any;
  im: any;

  currId: any;

  public showInput = false;
  public buttonName: any = 'Show';

  private scene: THREE.Scene;
  actionSheetController: any;

  constructor(private data: DataService,
              private vectormanager: VectorManagerService,
              private cd: ChangeDetectorRef) { }

  ngAfterViewInit() {
    this.cd.detectChanges();
  }

  receiveSwitchMode($event) {

    this.isDisabled = $event;
  }

  receiveOperation($event) {
    console.log('graphcomponent: received operations:' + $event);
    if (this.mode !== $event) {
      this.mode = $event;
      // this.clear();
    }
    this.operations = $event;
  }

  receiveInputs($event) {

    console.log('GraphComponent: received inputs:' + $event);
    if ($event[0] === 'modulus') {
      this.mod = $event[1];
    } else if ($event[0] === 'phase') {
      this.pha = $event[1];
    } else if ($event[0] === 'imaginary') {
      this.im = $event[1];
    } else {
      this.re = $event[1];
    }

    this.currId = $event[2];
  }

  receiveRoot($event) {
    this.root = $event;
    console.log('Root received into graph component: ' + this.root);
  }

  receiveScene($event) {

    this.scene = $event;
  }

  vectorEventReceived($event) {

    console.log('GraphComponent: vectorEvent received: ' + $event);

    switch ($event[1]) {

      case 'show':
        this.showVector($event[0]);
        break;

      case 'hide':
        this.hideVector($event[0]);
        break;

      case 'delete':
        this.deleteVector($event[0]);
        break;

      case 'flush':
        this.vectormanager.flush(this.scene);
        this.data.flush();
        try {

          this.sbstool.reset();
        } catch (e) { }
        console.log('flushed from graph comp');
    }

    switch ($event[1][0]) {
      case 'updateColor':
        this.updateColor($event[0], $event[1][1]);
        break;
      case 'updatePhaseColor':
        this.updatePhaseColor($event[0], $event[1][1]);
        break;
    }
  }

  showVector(event) {

    this.vectormanager.showVector(this.scene, event);
  }

  hideVector(event) {

    this.vectormanager.hideVector(this.scene, event);
  }

  deleteVector(event) {
    console.log('delete vecotr called from graph component with event: ' + event);
    this.vectormanager.removeVector(this.scene, event);
    this.data.deleteNumbers(event);
  }

  receiveButtonClicked($event) {

    console.log('graphcomponent: draw button received: ' + $event);

    switch ($event) {
      case 'Draw':
        this.drawVector();
        try {

          this.sbstool.reset();
        } catch (e) { }
        break;
      case 'Clear':
        this.clear();
        break;
      default: {
        this.operations = $event;
        this.operator = ($event === 0 ? '' : ($event === 1) ? '+' : ($event === 2) ? '-' : ($event === 3) ? '*' : '/');
        this.clear();
      }
               break;
    }
  }

  receiveDrawOptions($event) {

    this.drawOptions = $event;
  }

  drawVector() {

    try {
      this.updateList();
      switch (this.operations) {
        case 0:
          this.singleDisplay();
          break;
        case 1:
          this.addVectors();
          break;
        case 2:
          this.subVectors();
          break;
        case 3:
          this.multiplyVectors();
          break;
        case 4:
          this.divideVectors();
          break;
      }

    } catch (e) {}
  }

  singleDisplay() {

    this.scanOptions();
    this.drawNthRoot();
    this.show(this.data.getMap());
  }

  scanOptions() {

    if (this.drawOptions.length === 0) {
      this.inv = this.conj = false;
    } else if (this.drawOptions.length === 2) {
      this.inv = this.conj = true;
      this.drawConjugate();
      this.drawInverse();
    } else if (this.drawOptions[0] === 'i') {
      this.inv = true;
      this.conj = false;
      this.drawInverse();
    } else {
      this.inv = false;
      this.conj = true;
      this.drawConjugate();
    }
    this.show(this.data.getMap());
  }

  drawConjugate() {
    // mapped data are collected from data service through inputs
    const m = this.data.getMap();
    let r, im;
    let c = '';

    for (const [id, value] of m.entries()) {

      // input IDs are checked and result ID is selected
      if (!id.includes('result')) {
        c = id;
        // complex conjugate of a complex number is defined as follow :( if z = a + ib then conjugate of z is a - ib)
        r = value[0];
        im = - (value[1]);
      }
    }
    // check for the real part is not NaN
    if (!isNaN(r)) {
      // check if the same real or imaginary part is not already registered
      if (this.real !== r || this.imaginary !== im) {
        // asserting values to input components
        this.real = r;
        this.imaginary = im;
        // setting the polar form of the new complex conjugate values
        this.setPolar('c' + c);
        // adding the new vector to the vectorlist that is displayed on the cartesian graph
        // this.vectorlist.addItem(this.data.getNumbers());
        this.vectorlist.addItem(this.data.getNumbers());
      }
    }
  }

  drawInverse() {
    // mapped data are collected from data service through inputs
    const m = this.data.getMap();
    let r, im;
    let c = '';

    for (const [id, value] of m.entries()) {

      // input IDs are checked and result ID is selected
      if (!id.includes('result')) {
        c = id;
        // complex inverse of a complex number is defined as follow :
        // (if z is a non zero complex number then z^(-1) is Re(z) / b - Im(z) / b where b = Re(z)^2 + Im(z)^2)
        const b = Math.pow(value[0], 2) + Math.pow(value[1], 2);
        r = value[0] / b;
        im = - (value[1] / b);
      }
    }
    // check that z is not a non zero and that r is not a NaN
    if (!isNaN(r) && (this.real !== 0 && this.imaginary !== 0)) {
      if (this.real !== r || this.imaginary !== im) {
        // asserting new values to the real and imaginary part on their respective inputs
        this.real = r;
        this.imaginary = im;

        // setting the new values of the complex inverse into polar form
        this.setPolar('i' + c);
        // adding the new vector to the vectorlist that is displayed on the cartesian graph
        this.vectorlist.addItem(this.data.getNumbers());
      }
    }
  }

  drawNthRoot() {
    // check for valid values, when valid call calculate else set vectormanager.root to false
    if (this.root !== undefined
      && this.mod !== undefined
      && this.pha !== undefined) {
      this.vectormanager.root = true;
      this.calculate(this.root);
    }
    this.vectormanager.root = false;
  }

  calculate(n: number) {

    // temp variables declaration
    let a = 0, b = 0, m = 0, ph = 0;
    let p = 0;

    for (let k = 0; k < n; ++k) {

    // p = (theta / n) + 2 * k * PI
      p = (parseFloat(this.pha) / n) + k * (2 * Math.PI / n);
      // a = r^(1 / n) * cos(p)
      a = Math.pow(parseFloat(this.mod), (1 / n)) * Math.cos(p);
      // b = r^(1 / n) * sin(p)
      b = Math.pow(parseFloat(this.mod), (1 / n)) * Math.sin(p);
      // m = sqrt(a^2 + b^2)
      m = Math.sqrt((a * a) + (b * b));
      // ph = atan2(b, a)
      ph = Math.atan2(b, a);

      // drawing bounding circle on the nth root
      this.vectormanager.drawCirlcle(Math.pow(this.mod, 1 / n));
      // updating Data Service with new entry
      this.data.pushNumbers([ Number.parseFloat(a.toFixed(3)),
                              Number.parseFloat(m.toFixed(3)),
                              Number.parseFloat(b.toFixed(3)),
                              Number.parseFloat(ph.toFixed(3))]);
      // update View Options List
      this.updateList();
      // insert new nth root the the scene
      this.vectormanager.insertVector(a, b, true);
    }
  }

  show(map: Map<string, number[]>) {

    for (const value of map.values()) {

      this.vectormanager.insertVector(value[0], value[1]);
      this.vectormanager.injectVector(this.scene);
    }
  }

  updateList() {

    this.inputs.forEach(e => {
      e.update();
    });
    this.vectorlist.addItem(this.data.getNumbers());
    // this.data.flush();
  }

  resetCamera() {

    this.canvas.resetCameraPosition();
  }

  clear() {
    console.log('clear called');
    this.real = this.imaginary = this.modulus = this.phase = '';

    if (this.nthroot !== undefined) {
      this.nthroot.clear();
    }

    this.inputs.toArray().forEach(element => {
      element.flush();
    });

    this.vectorlist.clear();
  }

  addVectors() {

    this.real = this.imaginary = this.modulus = this.phase = '';
    const map = this.data.getMap();
    let v1;
    let v2;

    if (map.size > 4) {

      v1 = [...map.values()][map.size - 2];
      v2 = [...map.values()][map.size - 1];

    } else if (map.size === 4) {

      v1 = [...map.get('1')];
      v2 = [...map.get('2')];

    } else if (map.size === 3 && !map.has('result')) {

      v1 = [...map.values()][1];
      v2 = [...map.values()][2];

    } else {

      v1 = [...map.values()][0];
      v2 = [...map.values()][1];
    }


    this.real = parseFloat(v1[0].toString()) + parseFloat(v2[0].toString());
    this.imaginary = parseFloat(v1[1].toString()) + parseFloat(v2[1].toString());
    this.setPolar();
    this.show(map);
    this.updateList();
  }

  subVectors() {

    this.real = this.imaginary = this.modulus = this.phase = '';
    const map = this.data.getMap();
    let v1;
    let v2;

    if (map.size > 4) {

      v1 = [...map.values()][map.size - 2];
      v2 = [...map.values()][map.size - 1];

    } else if (map.size === 4) {

      v1 = [...map.get('1')];
      v2 = [...map.get('2')];

    } else if (map.size === 3 && !map.has('result')) {

      v1 = [...map.values()][1];
      v2 = [...map.values()][2];

    } else {

      v1 = [...map.values()][0];
      v2 = [...map.values()][1];
    }


    this.real = parseFloat(v1[0].toString()) - parseFloat(v2[0].toString());
    this.imaginary = parseFloat(v1[1].toString()) - parseFloat(v2[1].toString());

    this.setPolar();
    this.show(map);
    this.updateList();
  }

  multiplyVectors() {

    console.log('multiplyVectors called from GC');

    this.real = this.imaginary = this.modulus = this.phase = '';
    const map = this.data.getMap();
    console.log(map.size);
    console.log(this.data.getMap());


    let v1;
    let v2;

    if (map.size > 4) {

      v1 = [...map.values()][map.size - 2];
      v2 = [...map.values()][map.size - 1];

    } else if (map.size === 4) {

      v1 = [...map.get('1')];
      v2 = [...map.get('2')];

    } else if (map.size === 3 && !map.has('result')) {

      v1 = [...map.values()][1];
      v2 = [...map.values()][2];

    } else {

      v1 = [...map.values()][0];
      v2 = [...map.values()][1];
    }

    // var count = 0;

    // for (const e of map.keys()) {

    //   if (e.startsWith('result')) {
    //     ++count;
    //   }
    // }


    this.real = ((parseFloat(v1[0].toString()) * parseFloat(v2[0].toString()))
      - (parseFloat(v1[1].toString()) * parseFloat(v2[1].toString())));

    this.imaginary = (parseFloat(v1[0].toString()) * parseFloat(v2[1].toString()))
      + (parseFloat(v1[1].toString()) * parseFloat(v2[0].toString()));

    this.setPolar();
    this.show(map);
    this.updateList();
  }

  divideVectors() {

    this.real = this.imaginary = this.modulus = this.phase = '';
    const map = this.data.getMap();
    let v1;
    let v2;


    if (map.size > 4) {

      v1 = [...map.values()][map.size - 2];
      v2 = [...map.values()][map.size - 1];

    } else if (map.size === 4) {

      v1 = [...map.get('1')];
      v2 = [...map.get('2')];

    } else if (map.size === 3 && !map.has('result')) {

      v1 = [...map.values()][1];
      v2 = [...map.values()][2];

    } else {

      v1 = [...map.values()][0];
      v2 = [...map.values()][1];
    }

    const r = Math.pow(v2[0], 2) + Math.pow(v2[1], 2);
    this.real = (((v1[0] * v2[0]) + (v1[1] * v2[1])) / r).toFixed(3);
    this.imaginary = (((v1[1] * v2[0]) - (v1[0] * v2[1])) / r).toFixed(3);

    this.setPolar();
    this.show(map);
    this.updateList();
  }

  setPolar(s = null) {

    const m = parseInt(this.real) * parseInt(this.real) + parseInt(this.imaginary) * parseInt(this.imaginary);
    this.modulus = parseFloat(Math.sqrt(m).toFixed(4));
    const c = Math.atan2(this.imaginary, this.real);
    this.phase = parseFloat(c.toFixed(3));

    if (s !== null) {

      this.data.updateData('result ' + s,
        [parseFloat(parseFloat(this.real).toFixed(3)),
        parseFloat(parseFloat(this.imaginary).toFixed(3)),
        parseFloat(parseFloat(this.modulus).toFixed(3)),
        parseFloat(parseFloat(this.phase).toFixed(3))]);
    } else {

      this.data.updateData('result',
        [parseFloat(parseFloat(this.real).toFixed(3)),
        parseFloat(parseFloat(this.imaginary).toFixed(3)),
        parseFloat(parseFloat(this.modulus).toFixed(3)),
        parseFloat(parseFloat(this.phase).toFixed(3))]);
    }

    this.opnumbers = this.data.getMap();
  }

  toggle() {

    this.real = this.imaginary = this.modulus = this.phase = '';
    this.showInput = !this.showInput;
    if (this.showInput) {
      this.buttonName = 'Hide';
    } else {
      this.buttonName = 'Show';
    }
  }

  updateColor(i, value) {
    this.vectormanager.updateColor(this.scene, [i, value]);
  }

  updatePhaseColor(i, value) {
    this.vectormanager.updatePhaseColor(this.scene, [i, value]);
  }
}
