import { Component, OnInit, Input, EventEmitter, Output, OnDestroy, OnChanges } from '@angular/core';
import { DataService } from 'src/app/services/data.service';


let nextId = 0;

interface Set {
  id: string;
  values: number[];
}

@Component({
  selector: 'app-inputs',
  templateUrl: './inputs.component.html',
  styleUrls: ['./inputs.component.scss'],
})
export class InputsComponent implements OnChanges, OnDestroy {

  @Output() inputEvent = new EventEmitter<any[]>();

  @Input() real: any;
  @Input() imaginary: any;

  @Input() modulus: any;
  @Input() phase: any;

  @Input() isDisabled = false;

  @Input() id = `${nextId++}`;
  @Input() inputID;

  private set: number[];


  constructor(private data: DataService) {
    this.set = [];
  }

  ngOnChanges() {
    this.flush();
  }

  ngOnDestroy() {
    nextId = 0;
  }

  realChanged() {
    // validity check for the real number input
    if (this.real !== undefined && this.real !== '' && this.real !==  this.set[0] ) {
      this.set[0] = parseFloat(this.real);
      // validity check for the respective imaginary number
      if (this.imaginary !== undefined && this.imaginary !== '') {
        // conversion to polar
        this.toPolar();
      }
    } else {
      this.set[0] = parseFloat(this.real);
    }
    this.inputEvent.emit(['real', this.real, this.id]);
  }

  imChanged() {
        // validity check for the imaginary number input
    if (this.imaginary !== undefined && this.imaginary !== '' && this.imaginary !== this.set[1]) {
      this.set[1] = parseFloat(this.imaginary);
      // validity check for the respective real number
      if (this.real !== undefined && this.real !== '') {
        // conversion to polar form
        this.toPolar();
      }
    } else {
      this.set[1] = parseFloat(this.imaginary);
    }
    this.inputEvent.emit(['imaginary', this.imaginary]);
  }

  modChanged() {
    // validity check for the modulus of the complex number input
    if (this.modulus !== undefined && this.modulus !== '' && this.modulus !== this.set[2]) {
      this.set[2] = parseFloat(this.modulus);
      // validity check for the respective phase of the complex number
      if (this.phase !== undefined && this.phase !== '') {
        // conversion to cartesian form
        this.toCartesian();
      }
    } else {
      this.set[2] = parseFloat(this.modulus);
    }
    this.inputEvent.emit(['modulus', this.modulus]);
  }

  phaseChanged() {
    // validity check for the phase of the complex number input
    if (this.phase !== undefined && this.phase !== '' && this.phase !== this.set[3]) {
      this.set[3] = parseFloat(this.phase);
      // validity check for the respective modulus of the complex number
      if (this.modulus !== undefined && this.modulus !== '') {
        // conversion to cartesian form
        this.toCartesian();
      }
    } else {
      this.set[3] = parseFloat(this.phase);
    }
    this.inputEvent.emit(['phase', this.phase]);
  }

  toPolar() {
    // checking if the input component is not disabled for conversion
    if (!this.isDisabled) {
      this.set = [];
      // polar form of a complex number is calculated as follows: modulus = sqrt(Re(z)² + Im(z)² ))
      this.set[2] = parseFloat((Math.sqrt(Math.pow(this.real, 2) + Math.pow(this.imaginary, 2))).toFixed(4));
      // phase = arctan2(Im(z) / Re(z))
      const c = Math.atan2(this.imaginary, this.real);
      this.set[3] = parseFloat(c.toFixed(4));
      // asserting values to the modulus and phase inputs
      this.modulus = this.set[2];
      this.phase = this.set[3];
    }
  }

  toCartesian() {
    // checking if the input component is not disabled for conversion
    if (this.isDisabled) {
      this.set = [];
      // cartesian form of a complex number is defined as follows: Re(z) = mod(z) x cos(phase(z))
      this.set[0] = parseFloat((this.modulus * Math.cos(this.phase)).toFixed(4));
      // Im(z) = mod(z) x sin(phase(z))
      this.set[1] = parseFloat((this.modulus * Math.sin(this.phase)).toFixed(4));
      // asserting values to the real and imaginary numbers inputs
      this.real = this.set[0];
      this.imaginary = this.set[1];
    }
  }

  update() {

    const n = this.data.getNumbers();
    for (const sn of n) {
      if (sn === [this.real, this.imaginary, this.modulus, this.phase]) {
        return;
      }
    }
    const nset: Set = {
      values : [this.real, this.imaginary, this.modulus, this.phase],
      id : this.id
    };
    this.data.updateData(nset.id, nset.values);
  }

  flush() {
    this.real = this.imaginary = this.modulus = this.phase = '';
  }
}
