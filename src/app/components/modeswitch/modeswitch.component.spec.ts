import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModeswitchComponent } from './modeswitch.component';

describe('ModeswitchComponent', () => {
  let component: ModeswitchComponent;
  let fixture: ComponentFixture<ModeswitchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModeswitchComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModeswitchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
