import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-modeswitch',
  templateUrl: './modeswitch.component.html',
  styleUrls: ['./modeswitch.component.scss'],
})
export class ModeswitchComponent implements OnInit {

  isDisabled = false;

  @Output() switchEvent = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {}

  segmentChanged(ev: any) {
    this.isDisabled = !this.isDisabled;
    this.switchEvent.emit(this.isDisabled);
    console.log('switch emited: ' + this.isDisabled);
  }
}
