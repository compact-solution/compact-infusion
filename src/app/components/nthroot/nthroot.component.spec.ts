import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NthrootComponent } from './nthroot.component';

describe('NthrootComponent', () => {
  let component: NthrootComponent;
  let fixture: ComponentFixture<NthrootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NthrootComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NthrootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
