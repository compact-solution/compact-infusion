import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-nthroot',
  templateUrl: './nthroot.component.html',
  styleUrls: ['./nthroot.component.scss'],
})
export class NthrootComponent implements OnInit {

  root: any;
  isDisabled: true;

  @Output() rootEvent = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {}

  ngOnDestroy() {
    this.root = '';
    this.rootEvent.emit(this.root.toString());
  }

  rootChanged() {
    console.log('root changed called from nthroot component: ' + this.root);
    this.rootEvent.emit(this.root.toString());
  }

  clear() {
    this.root = '';
  }
}
