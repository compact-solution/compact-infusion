import { Component, OnInit, Output, EventEmitter, Input, OnChanges } from '@angular/core';
import * as stepJson from './steps.json';
import { Ratio } from 'lb-ratio';
import katex from 'katex';




@Component({
  selector: 'app-stepbysteptool',
  templateUrl: './stepbysteptool.component.html',
  styleUrls: ['./stepbysteptool.component.scss'],
})
export class StepByStepToolComponent implements OnChanges {

  constructor() { }
  currStep = -1;
  public show = false;
  data: any[4];

  @Input() currOperation: number;
  @Input() currVars: Map<string, number[]>;

  content: any;

  @Output() sbstool = new EventEmitter<string[]>();

  ngOnChanges() {
    this.currStep = -1;
    if (this.show) {
      this.nextItem();
    }
  }

  toggle() {

    this.show = !this.show;
    this.currStep = -1;
    if (!this.show) {
      document.getElementById('text').innerHTML = '';
    } else {
      this.nextItem();
    }
  }

  getIndex() {

    return this.data[0];
  }

  nextItem() {

    const opLength = this.currOperation === 1 ? stepJson.addition.length :
                    this.currOperation === 2 ? stepJson.subtraction.length :
                    this.currOperation === 3 ? stepJson.multiplication.length : stepJson.division.length;

    if (this.currStep < opLength - 1) {
      ++this.currStep;
      this.checkOperation();
      this.content = (this.content).replace(/\\\\/g, '\\');
      this.replaceElements();
      katex.render(this.content.replace(/"/g, ''), document.getElementById('text'), {throwOnError: false});
    }
  }

  prevItem() {

    if (this.currStep > 0) {
      --this.currStep;
      this.checkOperation();
      this.replaceElements();

      this.content = (this.content).replace(/\\\\/g, '\\');
      katex.render(this.content.replace(/"/g, ''), document.getElementById('text'), {throwOnError: false});
    }
  }

  checkOperation() {
    // clearing the current content string
    this.content = '';
    // currStep refers to the current step of the current operation
    if (this.currStep >= 0) {
    // use JSON.stringify method to retrieve string from .json file
    switch (this.currOperation) {

      case 1: // addition
          for (let i = 0; i <= this.currStep; ++i) {
            this.content += JSON.stringify(stepJson.addition[i]);
          }
          break;
      case 2: // subtraction
          for (let i = 0; i <= this.currStep; ++i) {
            this.content += JSON.stringify(stepJson.subtraction[i]);
          }
          break;
      case 3: // multiplication
          for (let i = 0; i <= this.currStep; ++i) {
            this.content += JSON.stringify(stepJson.multiplication[i]);
          }
          break;
      case 4: // division
          for (let i = 0; i <= this.currStep; ++i) {
            this.content += JSON.stringify(stepJson.division[i]);
          }
          break;
      }
    }
  }

  replaceElements() {
    console.log('replaceElement called from sbstool');
    console.log(this.currVars);

    if (this.currVars !== undefined && this.currVars.size > 0) {

      let v1, v2, v3;

      if (this.currVars.size > 4) {
        v1 = [...this.currVars.values()][this.currVars.size - 2];
        v2 = [...this.currVars.values()][this.currVars.size - 1];
        v3 = [...this.currVars.get('result')];
      } else if (this.currVars.size ===  4) {
        v1 = [...this.currVars.get('1')];
        v2 = [...this.currVars.get('2')];
        v3 = [...this.currVars.get('result')];
      }  else {
        v1 = [...this.currVars.values()][0];
        v2 = [...this.currVars.values()][1];
        v3 = [...this.currVars.values()][2];
      }


      this.content = this.content.replace(new RegExp('{a}', 'g'), (v1[0] >= 0 ? v1[0] : '(' + v1[0] + ')'));
      this.content = this.content.replace(new RegExp('{b}', 'g'), (v1[1] >= 0 ? v1[1] : '(' + v1[1] + ')'));
      this.content = this.content.replace(new RegExp('{c}', 'g'), (v2[0] >= 0 ? v2[0] : '(' + v2[0] + ')'));
      this.content = this.content.replace(new RegExp('{d}', 'g'), (v2[1] >= 0 ? v2[1] : '(' + v2[1] + ')'));
      this.content = this.content.replace(new RegExp('{e}', 'g'), (v3[0] >= 0 ? v3[0] : '(' + v3[0] + ')'));
      this.content = this.content.replace(new RegExp('{f}', 'g'), (v3[1] >= 0 ? v3[1] : '(' + v3[1] + ')'));
      this.content = this.content.replace(new RegExp('{\-}', 'g'), (v3[1] >= 0 ? '+' : '\\;'));

      if (this.currOperation === 4) {
        const j = Math.pow(v2[0], 2) + Math.pow(v2[1], 2);
        const g = v1[0] * v2[0] + v1[1] * v2[1];
        const h = v1[1] * v2[0] - v1[0] * v2[1];
        const gr = Ratio(g, j).simplify();
        const hr = Ratio(h, j).simplify();
        this.content = this.content.replace(new RegExp('{g}', 'g'), (g >= 0 ? g : '(' + g + ')'));
        this.content = this.content.replace(new RegExp('{h}', 'g'), (h >= 0 ? h : '(' + h + ')'));
        this.content = this.content.replace(new RegExp('{j}', 'g'), (j >= 0 ? j : '(' + j + ')'));
        this.content = this.content.replace(new RegExp('{gr}', 'g'), (gr.denominator() === 1 ? gr.numerator() :
                                gr.numerator() === 0 ? 0 : '\\frac{' + gr.numerator() + '}{' + gr.denominator() + '}'));
        this.content = this.content.replace(new RegExp('{hr}', 'g'), (hr.denominator() === 1 ? hr.numerator() :
                                gr.numerator() === 0 ? 0 : '\\frac{' + hr.numerator() + '}{' + hr.denominator() + '}'));
        this.content = this.content.replace(new RegExp('{l}', 'g'), '\\\\ \\\\');
      }
    } else {
      this.content = this.content.replace(new RegExp('hr', 'g'), '');
      this.content = this.content.replace(new RegExp('gr', 'g'), '');
      this.content = this.content.replace(new RegExp('hr', 'g'), '');
      this.content = this.content.replace(new RegExp('{\\+}', 'g'), '');
      this.content = this.content.replace(new RegExp('hr', 'g'), '');
      this.content = this.content.replace(new RegExp('{i}', 'g'), '');
      this.content = this.content.replace(new RegExp('{=}', 'g'), '');
      this.content = this.content.replace(new RegExp('{l}', 'g'), '');
      // this.content = this.content.replace(new RegExp('{\\\\ \\\\}', 'g'), '');
    }
  }

  reset() {
    this.currStep = 0;
    document.getElementById('text').innerHTML = '';
  }
}
