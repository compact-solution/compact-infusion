import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VectormanagerComponent } from './vectormanager.component';

describe('VectormanagerComponent', () => {
  let component: VectormanagerComponent;
  let fixture: ComponentFixture<VectormanagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VectormanagerComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VectormanagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
