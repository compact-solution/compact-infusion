import { Component, OnInit, EventEmitter, Output, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-vectormanager',
  templateUrl: './vectormanager.component.html',
  styleUrls: ['./vectormanager.component.scss'],
})
export class VectormanagerComponent implements OnInit, AfterViewInit {

  data: any[];
  color: any[];
  pColor: any[];

  checked: boolean[];

  @Output() vectorEvent = new EventEmitter<string[]>();

  public show = false;
  public buttonName: any = 'Show';

  constructor(private cd: ChangeDetectorRef,
              private actionSheetController: ActionSheetController) {

    this.data = [];
    this.checked = [];
    this.color = [];
    this.pColor = [];
  }

  ngOnInit() {

    this.cd.detectChanges();
  }

  ngAfterViewInit() {

    this.cd.detectChanges();
  }

  addItem(item) {

      this.color = [];
      this.pColor = [];
      this.data = [];
      this.checked = [];
      let i = 0;
      for (const element of item) {
        this.data.push(element);
        this.checked[i++] = false;
      }
  }

  hide(i) {


    if (!this.checked[i]) {
      this.vectorEvent.emit([i, 'hide']);
    } else {
      this.vectorEvent.emit([i, 'show']);
    }
    this.checked[i] = !this.checked[i];
  }

  deleteItem(i) {
    console.log('deleteItem called with index:' + i);

    if (this.data.length > 1) {
      // this.checked[i] = !this.checked[i];
      this.data.splice(i, 1);
      this.checked.splice(i, 1);
    } else {
      this.data = [];
      this.checked = [];
    }
    setTimeout(() => this.vectorEvent.emit([i, 'delete']));
  }

  clear() {
    console.log('Clear: called');
    if (!this.show) {
      console.log('Clear: if');
      for ( let i = 0; i < this.data.length; ++i) {
        console.log('Clear: for: ' + this.data.length);
        console.log('Clear: checked: ' + this.checked);

        if (!this.checked[i]) {
          this.hide(i);
        }
      }
    } else {
      console.log('Clear: else');
      const evt = document.createEvent('MouseEvents');
      evt.initMouseEvent('click', true, true, window,
      0, 0, 0, 0, 0, false, false, false, false, 0, null);
      const cb = document.getElementsByClassName('toggle');
      let i = 0;
      let res = Array.prototype.filter.call(cb, e => {
        if (e.checked === true) {
          e.dispatchEvent(evt);
        }
      });
    }
  }

  toggle() {

    this.show = !this.show;

    if (this.show) {
      this.buttonName = 'Hide';
    } else {
      this.buttonName = 'Show';
    }
  }

  randomizeColors(i: number) {
    let randomColor = (Math.random() * 0xfffff * 1000000).toString(16);
    this.colorSelected(i, '#' + randomColor.slice(0, 6));
    this.color[i] = '#' + randomColor.slice(0, 6);
    randomColor = (Math.random() * 0xfffff * 1000000).toString(16);
    this.phaseColorSelected(i, '#' + randomColor.slice(0, 6));
    this.pColor[i] = '#' + randomColor.slice(0, 6);
  }

  colorSelected(i, value: string) {

    this.vectorEvent.emit([i, ['updateColor', value]]);
  }

  updateColor(i, value: string) {
    this.color[i] = value;
  }

  updatePhaseColor(i, value: string) {
    this.pColor[i] = value;
  }

  phaseColorSelected(i, value: string) {

    this.vectorEvent.emit([i, ['updatePhaseColor', value]]);
  }

  flush() {
    for (let i = 0; i < this.data.length; ++i) {
      this.deleteItem(i);
    }
    this.data = [];
    this.checked = [];
    this.vectorEvent.emit(['', 'flush']);
  }

  async presentActionSheet() {

    const actionSheet = await this.actionSheetController.create({
      header: 'This will delete all present all objects on the scene, proceed?',
      cssClass: 'operationSheet',
      buttons: [{
        text: 'Yes',
        icon: 'checkmark',
        handler: () => {
          this.flush();
        }
      }, {
        text: 'No',
        icon: 'close',
        handler: () => {

        }
      }, {
        text: 'Return',
        icon: 'return-left',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
    }
}

