import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GraphPageRoutingModule } from './graph-routing.module';
import { ComponentsModule } from '../../components/components.module';

import { GraphPage } from './graph.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    GraphPageRoutingModule
  ],
  declarations: [GraphPage]
})
export class GraphPageModule {}
