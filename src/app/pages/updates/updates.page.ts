import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { UpdateService } from '../../services/update.service';


@Component({
  selector: 'app-updates',
  templateUrl: './updates.page.html',
  styleUrls: ['./updates.page.scss'],
})
export class UpdatesPage implements OnInit {

  bcolor = 'warning';

  constructor(public navCtrl: NavController,
              private update: UpdateService) {}

  ngOnInit() {
  }

  updateClient() {

    if (this.update.isUpdated()) {
      this.bcolor = 'success';
      document.getElementById('uc').innerHTML = 'C-Numbers has been updated to the latest Version.';
    } else {
      setTimeout( () => {
        this.bcolor = 'primary';
        document.getElementById('uc').innerHTML = 'you are on the latest version';
      }, 1000);
      setTimeout(() => location.reload() , 4000);
    }
  }

  async checkUpdates() {

    this.update.checkForUpdate();
    this.updateClient();
  }
}
