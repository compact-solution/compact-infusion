import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private map: Map<string, number[]>;
  private dataSource = new BehaviorSubject<number[]>([]);
  currentData = this.dataSource.asObservable();
  private currentIds: string[];
  private numbers: number[][];

  constructor() {

    this.map = new Map<string, number[]>();
    this.numbers = [];
   }

  updateData(id: string, values: number[]) {

    for ( let i = 0; i < values.length; ++i) {
      if (values[i].toString() === '') {
        return;
      }
    }

    if ((this.map.get(id) !== values)) {
      this.map.set(id, values);
      this.pushNumbers(values);
    }

    this.currentIds = [];
    for (const [id, value] of this.map.entries()) {
        this.currentIds.push(id);
      }
  }

  flush() {

    this.numbers = [];
    this.map.clear();
  }

  pushNumbers(n: number[]) {
    console.log('pushnumberscalled:');
    console.log(this.map);
    console.log(this.numbers);
    console.log(n);

    // if (this.numbers.length === 0) {

    //   this.numbers.push(n);
    // } else {
    //   let isDouble = false;
    //   this.numbers.forEach(e => {

    //     if (e[0] === n[0] && e[1] === n[1]) {
    //       isDouble = true;
    //     }
    //   });

    //   if (!isDouble) {
    //     this.numbers.push(n);
    //   }
    // }



    n.forEach(e => {
      if ( e.toString() === '') {
        return;
      }
    });

    if (this.numbers !== null) {

      for (const e of this.numbers) {
        if ( e[0] === n[0] && e[1] === n[1]) {
          return;
        }
      }
      this.numbers.push(n);
    } else {
      this.numbers.push(n);
    }
  }

getNumbers() {

  return this.numbers;
  }

deleteNumbers($event) {

  if (this.numbers.length > 1) {
    for ( let id of this.map.keys() ) {
      if (this.map.get(id) === this.numbers[$event]) {
        this.map.delete(id);
      }
    }
    this.numbers.splice($event, 1);
  } else {
    this.map = new Map<string, number[]>();
    this.numbers = [];
  }
}

  getMap() {

    return this.map;
  }
}
