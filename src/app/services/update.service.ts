import { Injectable } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';

@Injectable({
  providedIn: 'root'
})
export class UpdateService {

  updated = false;

  constructor(private updates: SwUpdate) {
    updates.available.subscribe(event => {
      if (confirm('update available')) {
        updates.activateUpdate().then(() => {
          this.updated = true;
          document.location.reload();
        });
      } else {
        this.updated = false;
      }
    });

    this.updates.activated.subscribe(event => {
      console.log('old version was', event.previous);
      console.log('new version is', event.current);
    });
  }

  isUpdated() {

    return this.updated;
  }

  checkForUpdate() {

    this.updates.checkForUpdate();
  }
}
