import { TestBed } from '@angular/core/testing';

import { VectorManagerService } from './vectormanager.service';

describe('VectormanagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VectorManagerService = TestBed.get(VectorManagerService);
    expect(service).toBeTruthy();
  });
});
