import { Injectable } from '@angular/core';
import * as THREE from 'three';
import { DataService } from './data.service';
import { Object3D } from 'three';
import { Text } from 'troika-three-text';

@Injectable({
  providedIn: 'root'
})
export class VectorManagerService {

  static p = 0.0005;

  private oldValue: any [][];
  private valuesChanged = false;
  root = false;

  private vector: THREE.Group;
  private vectors: any[];
  private visibleVectors: Object3D[];

  constructor(private data: DataService) {
    this.vector = new THREE.Group();
    this.vectors = [];
    this.visibleVectors = [];
  }

  insertVector(x: number, y: number, b?: boolean) {
    // first iteration, push to oldValue and set valuesChanged to true, then call addMaterial and saveVector
    if (this.oldValue === undefined) {
      this.oldValue = [];
      this.oldValue.push([x, y]);
      this.valuesChanged = true;
      this.addMaterial(x, y, b);
      this.saveVector();
    } else {
    // iterate through oldValue, if value set is present return
      let i = 0;
      for ( ; i < this.oldValue.length; ++i) {
        if (this.oldValue[i][0] === x && this.oldValue[i][1] === y) {
          this.valuesChanged = false;
          return;
        }
      }
      // push to oldValue, set valuesChanged to true, then call addMaterial and saveVector
      this.oldValue.push([x, y]);
      this.valuesChanged = true;
      this.addMaterial(x, y, b);
      this.saveVector();
    }
  }

  saveVector() {
    //  valuesChanged must be true
    if (this.valuesChanged) {
      if (!this.vectors.includes(this.vector)) {
        // if current vector is not included push to visibleVectors and vectors arrays
        this.visibleVectors.push(this.vector);
        this.vectors.push(this.vector);
      }
    }
    this.vector = new THREE.Group();
  }

  injectVector(scene: THREE.Scene) {
    // if visibleVectors is greater or equal to 1, remove all vectors from the scene then add all visibleVectors
    if (this.visibleVectors.length >= 1) {
     this.vectors.forEach(e => {
       scene.remove(e);
     });
     this.visibleVectors.forEach( e => {
       scene.add(e);
     });
   }
 }

  addMaterial(x: number, y: number, b?: boolean) {

    // if (x !== 0 || y !== 0) {
    //   const Xpoints = [];
    //   Xpoints.push( new THREE.Vector3( x, 0.0005, 0 ) );
    //   Xpoints.push( new THREE.Vector3( x, 0.0005, -y ) );

    //   const Xline = new THREE.LineSegments( new THREE.BufferGeometry().setFromPoints(Xpoints) ,
    //     new THREE.LineDashedMaterial( {
    //     color: 0xffffff,
    //     linewidth: 2,
    //     scale: 1,
    //     dashSize: 0.4,
    //     gapSize: 0.2
    //     })
    //   );

    //   const Ypoints = [];
    //   Xpoints.push( new THREE.Vector3( 0, 0.0005, -y ) );
    //   Xpoints.push( new THREE.Vector3( x, 0.0005, -y ) );

    //   const Yline = new THREE.LineSegments( new THREE.BufferGeometry().setFromPoints(Xpoints) ,
    //     new THREE.LineDashedMaterial( {
    //     color: 0xffffff,
    //     linewidth: 2,
    //     scale: 1,
    //     dashSize: 0.4,
    //     gapSize: 0.2
    //     })
    //   );

    //   Xline.computeLineDistances();
    //   Yline.computeLineDistances();
    //   this.vector.add(Yline);
    // }

    if (!b) {
      this.addCurve(x, y);
    }

    this.addArrow(x, y);
    if (!this.root) {
      this.loadText(x, y);
    }
  }


  loadText(x: number, y: number) {

    const text = new Text();

    text.text = '(' + x  + ' , ' + y + ')';
    text.fontSize = .5;
    text.position.y = 0;
    text.position.z = y > 0 ? -y - .5 : -y; // neg up , pos down
    text.position.x = x; // left right
    text.rotation.x = - Math.PI / 2;
    text.color = 0x000000;
    text.sync();
    this.vector.add(text);
  }

  addArrow(x: number, y: number) {

    const points = [];
    points.push( new THREE.Vector3( 0, 0, 0 ) );
    points.push( new THREE.Vector3( x, 0, - y));

    const line = new THREE.Line( new THREE.BufferGeometry().setFromPoints( points),
    new THREE.LineBasicMaterial( {
      color: 0x2155da,
      linewidth: 2
      })
    );

    const dir = new THREE.Vector3( x, 0, (-1) * y);
    const origin = new THREE.Vector3( 0 , 0, 0 );
    const length = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    const hex = 0x2155da;
    const arrowHelper = new THREE.ArrowHelper( dir, origin, length, hex );

    this.vector.add(line);
    this.vector.add(arrowHelper);
  }

  addCurve(x: number, y: number) {

    const theta = (x > 0) ? Math.atan2(y, x) : (Math.atan2(y, x) + Math.PI);
    const nmaterial = new THREE.MeshBasicMaterial( {
      color: 0x36d52c,
      opacity: 0.5,
      transparent: true } );
    let ngeometry = new THREE.CircleGeometry(
      x / 3,
      32,
      0,
      theta
    );

    if ( y < 0 && x > 0) {
      ngeometry.rotateX(1.5 * Math.PI );
      ngeometry.rotateZ(Math.PI - VectorManagerService.p);
      ngeometry.rotateY(theta + Math.PI);

    } else if (x < 0 && y >= 0) {
      ngeometry = new THREE.CircleGeometry(
        x / 3,
        32,
        0,
        theta - Math.PI
      );
      ngeometry.rotateX(1.5 * Math.PI - VectorManagerService.p);
      ngeometry.rotateY(Math.PI);

    } else if ( x < 0 && y <= 0) {
      ngeometry = new THREE.CircleGeometry(
        x / 3,
        32,
        0,
        theta - Math.PI
      );
      ngeometry.rotateX(0.5 * Math.PI - VectorManagerService.p);
      ngeometry.rotateY(theta);
    } else {
      ngeometry.rotateX(1.5 * Math.PI );
      ngeometry.rotateZ(VectorManagerService.p);
    }

    const circle = new THREE.Mesh( ngeometry, nmaterial );
    this.vector.add( circle );
    VectorManagerService.p -= .0001;
  }

  hideVector(scene: THREE.Scene, index: number) {
    console.log('hidevector from vm service called with index:' + index);
    console.log('vectors:');
    console.log(this.vectors);
    console.log('visibleVectors');
    console.log(this.visibleVectors);

    for (let i = 0; i < this.visibleVectors.length; ++i) {
      if (this.visibleVectors[i] === this.vectors[index]) {
        scene.remove(this.visibleVectors[i]);
        this.visibleVectors.splice(i, 1);
      }
    }
    console.log('finished');
    console.log('vectors:');
    console.log(this.vectors);
    console.log('visibleVectors');
    console.log(this.visibleVectors);
  }

  isAbove(v1: Object3D, v2: Object3D) {

    return (this.vectors.indexOf(v1) > this.vectors.indexOf(v2));
  }

  showVector(scene: THREE.Scene, index: number) {
    console.log('showvector from vm service called with index:' + index);
    console.log('vectors:');
    console.log(this.vectors);
    console.log('visibleVectors');
    console.log(this.visibleVectors);
    if (this.vectors.length >= 1) {

      const v2 = this.vectors[index];
      let i = 0;

      for (const v of this.visibleVectors) {
        if (!this.isAbove(v2, v)) {
          break;
        }
        ++i;
      }
      this.visibleVectors.splice(i, 0, v2);
      this.injectVector(scene);
    }

    console.log('finished');
    console.log('vectors:');
    console.log(this.vectors);
    console.log('visibleVectors');
    console.log(this.visibleVectors);
  }

  removeVector(scene: THREE.Scene, index: number) {
    console.log('removeVector from vm service called with index:' + index);
    console.log('vectors:');
    console.log(this.vectors);
    console.log('visibleVectors');
    console.log(this.visibleVectors);

    for (let i = 0; i < this.visibleVectors.length; ++i) {
        if (this.vectors[index] === this.visibleVectors[i]) {
          this.hideVector(scene, i);
        }
    }
    this.vectors.splice(index, 1);
    this.oldValue.splice(index, 1);

    console.log('finished');
    console.log('vectors:');
    console.log(this.vectors);
    console.log('visibleVectors');
    console.log(this.visibleVectors);
  }

  updateColor(scene: THREE.Scene, values: string[]) {

    const hexToRgb = hex =>
          hex.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i
             , (m, r, g, b) => '#' + r + r + g + g + b + b)
            .substring(1).match(/.{2}/g)
            .map(x => parseInt(x, 16));

    const rgbcolor = hexToRgb(values[1]);

    rgbcolor[0] = rgbcolor[0] / 255;
    rgbcolor[1] = rgbcolor[1] / 255;
    rgbcolor[2] = rgbcolor[2] / 255;

    this.vectors[values[0]].children[2].children[0].material.color.r = rgbcolor[0] ;
    this.vectors[values[0]].children[2].children[1].material.color.r = rgbcolor[0] ;
    this.vectors[values[0]].children[2].children[0].material.color.g = rgbcolor[1];
    this.vectors[values[0]].children[2].children[1].material.color.g = rgbcolor[1];
    this.vectors[values[0]].children[2].children[0].material.color.b = rgbcolor[2];
    this.vectors[values[0]].children[2].children[1].material.color.b = rgbcolor[2];

    this.vectors[values[0]].children[1].material.color.r = rgbcolor[0];
    this.vectors[values[0]].children[1].material.color.g = rgbcolor[1];
    this.vectors[values[0]].children[1].material.color.b = rgbcolor[2];

    this.injectVector(scene);
  }

  updatePhaseColor(scene: THREE.Scene, values: string[]) {

    const hexToRgb = hex =>
          hex.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i
             , (m, r, g, b) => '#' + r + r + g + g + b + b)
            .substring(1).match(/.{2}/g)
            .map(x => parseInt(x, 16));

    const rgbcolor = hexToRgb(values[1]);
    rgbcolor[0] = rgbcolor[0] / 255;
    rgbcolor[1] = rgbcolor[1] / 255;
    rgbcolor[2] = rgbcolor[2] / 255;

    this.vectors[values[0]].children[0].material.color.r = rgbcolor[0];
    this.vectors[values[0]].children[0].material.color.g = rgbcolor[1];
    this.vectors[values[0]].children[0].material.color.b = rgbcolor[2];

    this.injectVector(scene);
  }

  drawCirlcle(r: any) {
    const geometry = new THREE.CircleGeometry( r, 64 );
    geometry.rotateX(- Math.PI / 2);
    const material = new THREE.MeshBasicMaterial( {
      color: 0xffbb29,
      opacity: .1,
      transparent: true } );
    const circle = new THREE.Mesh( geometry, material );
    circle.position.y = -.01;
    this.vector.add(circle);
  }

  flush(scene: THREE.Scene) {


    console.log('VectorManager: flush called');
    this.vectors.forEach(e => {
      scene.remove(e);
    });
    this.visibleVectors = [];
    this.vectors = [];
    this.oldValue = [];

  }

  delete($event) {

    console.log('VectorManager: vectorlist updated');
    console.log(this.getVectors);
  }

  getVectors() {

    return this.vectors;
  }

  getMappedValues() {

    const c = this.data.getMap();
    console.log(c);
  }
}
