console.log("Webpack Initializing plugins");

const zlib = require('zlib');
// const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
// const MinifyPlugin = require("babel-minify-webpack-plugin");
const TerserPlugin = require('terser-webpack-plugin');


module.exports = {
    optimization: {
      splitChunks: {
        chunks: "all",
        maxInitialRequests: 20, // for HTTP2
        maxAsyncRequests: 20, // for HTTP2
        minSize: 40 // for example only: chosen to match 2 modules
        // omit minSize in real use case to use the default of 30kb
      },
        minimize: true,
        minimizer: [
          new TerserPlugin({
            cache: true
          }),
        ],
      },
  plugins: [
    // new MinifyPlugin({}, {}),
    new HtmlWebpackPlugin({
        title: 'Output Management',
    }),
    new webpack.optimize.LimitChunkCountPlugin({
        maxChunks: 10
    }),
    new CompressionPlugin({
      filename: '[path].br[query]',
      algorithm: 'brotliCompress',
      test: /\.(js|css|html|svg)$/,
      compressionOptions: {
        level: 20,
      },
      threshold: 10240,
      minRatio: 0.8,
      filename(pathData) {
        // The `pathData` argument contains all placeholders - `path`/`name`/`ext`/etc
        // Available properties described above, for the `String` notation
        if (/\.svg$/.test(pathData.file)) {
          return "assets/svg/[path][base].gz";
        }
 
        return "assets/js/[path][base].gz";
      },
    }),
  ],
};